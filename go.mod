module gitlab.com/aslatter/go-hash-bench

go 1.21.3

require (
	github.com/zeebo/blake3 v0.2.3
	golang.org/x/crypto v0.14.0
	lukechampine.com/blake3 v1.2.1
)

require (
	github.com/klauspost/cpuid/v2 v2.0.12 // indirect
	golang.org/x/sys v0.13.0 // indirect
)

package hashbench

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"testing"

	zblake3 "github.com/zeebo/blake3"
	"golang.org/x/crypto/sha3"
	lblake3 "lukechampine.com/blake3"
)

func BenchmarkSha1_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha1result = sha1.Sum(testdata_long)
	}
}

func BenchmarkSha256_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha256result = sha256.Sum256(testdata_long)
	}
}

func BenchmarkSha256_224_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha256_224result = sha256.Sum224(testdata_long)
	}
}

func BenchmarkSha512_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512result = sha512.Sum512(testdata_long)
	}
}

func BenchmarkSha512_384_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512_384result = sha512.Sum384(testdata_long)
	}
}

func BenchmarkSha512_224_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512_224result = sha512.Sum512_224(testdata_long)
	}
}

func BenchmarkSha512_256_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512_256result = sha512.Sum512_256(testdata_long)
	}
}

func BenchmarkSha3_224_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_224result = sha3.Sum224(testdata_long)
	}
}

func BenchmarkSha3_256_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_256result = sha3.Sum256(testdata_long)
	}
}

func BenchmarkSha3_384_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_384result = sha3.Sum384(testdata_long)
	}
}

func BenchmarkSha3_512_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_512result = sha3.Sum512(testdata_long)
	}
}

func BenchmarkZeeboBlake3_256_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_256result = zblake3.Sum256(testdata_long)
	}
}

func BenchmarkZeeboBlake3_512_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_512result = zblake3.Sum512(testdata_long)
	}
}

func BenchmarkLCBlake3_256_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_256result = lblake3.Sum256(testdata_long)
	}
}

func BenchmarkLCBlake3_512_long(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_512result = lblake3.Sum512(testdata_long)
	}
}

package hashbench

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"testing"

	zblake3 "github.com/zeebo/blake3"
	"golang.org/x/crypto/sha3"
	lblake3 "lukechampine.com/blake3"
)

var testdata_short []byte
var testdata_long []byte

func init() {
	for i := 0; i < 3; i++ {
		testdata_short = append(testdata_short, "the quick brown fox jumps over the lazy dog. "...)
	}
	for i := 0; i < 1000; i++ {
		testdata_long = append(testdata_long, "the quick brown fox jumps over the lazy dog. "...)
	}
}

var sha1result [20]byte

func BenchmarkSha1_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha1result = sha1.Sum(testdata_short)
	}
}

var sha256result [sha256.Size]byte

func BenchmarkSha256_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha256result = sha256.Sum256(testdata_short)
	}
}

var sha256_224result [sha256.Size224]byte

func BenchmarkSha256_224_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha256_224result = sha256.Sum224(testdata_short)
	}
}

var sha512result [sha512.Size]byte

func BenchmarkSha512_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512result = sha512.Sum512(testdata_short)
	}
}

var sha512_384result [48]byte

func BenchmarkSha512_384_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512_384result = sha512.Sum384(testdata_short)
	}
}

var sha512_224result [28]byte

func BenchmarkSha512_224_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512_224result = sha512.Sum512_224(testdata_short)
	}
}

var sha512_256result [32]byte

func BenchmarkSha512_256_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha512_256result = sha512.Sum512_256(testdata_short)
	}
}

var sha3_224result [28]byte

func BenchmarkSha3_224_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_224result = sha3.Sum224(testdata_short)
	}
}

var sha3_256result [32]byte

func BenchmarkSha3_256_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_256result = sha3.Sum256(testdata_short)
	}
}

var sha3_384result [48]byte

func BenchmarkSha3_384_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_384result = sha3.Sum384(testdata_short)
	}
}

var sha3_512result [64]byte

func BenchmarkSha3_512_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sha3_512result = sha3.Sum512(testdata_short)
	}
}

var blake3_256result [32]byte

func BenchmarkZeeboBlake3_256_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_256result = zblake3.Sum256(testdata_short)
	}
}

var blake3_512result [64]byte

func BenchmarkZeeboBlake3_512_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_512result = zblake3.Sum512(testdata_short)
	}
}

func BenchmarkLCBlake3_256_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_256result = lblake3.Sum256(testdata_short)
	}
}

func BenchmarkLCBlake3_512_short(b *testing.B) {
	for i := 0; i < b.N; i++ {
		blake3_512result = lblake3.Sum512(testdata_short)
	}
}

This package benchmarks a collection of cryptographic hash functions
in Go.

Most of these are from the Go standrad library or from golang.org/x/crypto.

We additionally include the BLAKE3 implementations from github.com/zeebo/blake3
and lukechampine.com/blake3. 

```
$ go test -benchmem -bench .
goos: linux
goarch: amd64
pkg: gitlab.com/aslatter/go-hash-bench
cpu: AMD Ryzen 7 5800X 8-Core Processor
BenchmarkSha1_long-16                      44715             26561 ns/op               0 B/op          0 allocs/op
BenchmarkSha256_long-16                    63559             18897 ns/op               0 B/op          0 allocs/op
BenchmarkSha256_224_long-16                63560             18884 ns/op               0 B/op          0 allocs/op
BenchmarkSha512_long-16                    27234             44066 ns/op               0 B/op          0 allocs/op
BenchmarkSha512_384_long-16                26785             44133 ns/op               0 B/op          0 allocs/op
BenchmarkSha512_224_long-16                26712             44286 ns/op               0 B/op          0 allocs/op
BenchmarkSha512_256_long-16                27226             44066 ns/op               0 B/op          0 allocs/op
BenchmarkSha3_224_long-16                  14290             82786 ns/op             928 B/op          3 allocs/op
BenchmarkSha3_256_long-16                  13692             88080 ns/op             928 B/op          3 allocs/op
BenchmarkSha3_384_long-16                  10000            115051 ns/op             944 B/op          3 allocs/op
BenchmarkSha3_512_long-16                   7152            163596 ns/op             960 B/op          3 allocs/op
BenchmarkZeeboBlake3_256_long-16          133456              8993 ns/op               0 B/op          0 allocs/op
BenchmarkZeeboBlake3_512_long-16          133458              8977 ns/op               0 B/op          0 allocs/op
BenchmarkLCBlake3_256_long-16              74936             15938 ns/op               0 B/op          0 allocs/op
BenchmarkLCBlake3_512_long-16              76347             15936 ns/op               0 B/op          0 allocs/op
BenchmarkSha1_short-16                   4913229               244.3 ns/op             0 B/op          0 allocs/op
BenchmarkSha256_short-16                10159292               117.7 ns/op             0 B/op          0 allocs/op
BenchmarkSha256_224_short-16             9836917               121.7 ns/op             0 B/op          0 allocs/op
BenchmarkSha512_short-16                 4067034               297.0 ns/op             0 B/op          0 allocs/op
BenchmarkSha512_384_short-16             4028017               294.3 ns/op             0 B/op          0 allocs/op
BenchmarkSha512_224_short-16             4058553               297.1 ns/op             0 B/op          0 allocs/op
BenchmarkSha512_256_short-16             4091515               293.1 ns/op             0 B/op          0 allocs/op
BenchmarkSha3_224_short-16               2610012               460.6 ns/op           928 B/op          3 allocs/op
BenchmarkSha3_256_short-16               2638377               454.5 ns/op           928 B/op          3 allocs/op
BenchmarkSha3_384_short-16               1541588               780.2 ns/op           944 B/op          3 allocs/op
BenchmarkSha3_512_short-16               1609352               748.4 ns/op           960 B/op          3 allocs/op
BenchmarkZeeboBlake3_256_short-16        7147356               164.4 ns/op             0 B/op          0 allocs/op
BenchmarkZeeboBlake3_512_short-16        7354512               159.6 ns/op             0 B/op          0 allocs/op
BenchmarkLCBlake3_256_short-16           4596206               264.8 ns/op             0 B/op          0 allocs/op
BenchmarkLCBlake3_512_short-16           4593652               258.0 ns/op             0 B/op          0 allocs/op
PASS
ok      gitlab.com/aslatter/go-hash-bench       45.588s
```

